import React from 'react';
import GoBack from '../../components/GoBack';

import './musicPage.scss';


const musicPage = () =>{
    return(
        
        <div className="search">
        <GoBack />
            <h1 className='search__title'>Top 15 de España</h1>
        <div className="search__music">
        
          <iframe
            className="search__iframe"
            src="https://open.spotify.com/embed/track/52LJ3hyknOijCrE5gCD0rE"
            width="250"
            height="250"
            allowtransparency="true"
            allow="encrypted-media"
          ></iframe>
          
  
          <iframe
            className="search__iframe"
            src="https://open.spotify.com/embed/track/04zrPERrSO4CfyargthKJ9"
            width="250"
            height="250"
            allowtransparency="true"
            allow="encrypted-media"
          ></iframe>
  
          <iframe
            className="search__iframe"
            src="https://open.spotify.com/embed/track/2XIc1pqjXV3Cr2BQUGNBck"
            width="250"
            height="250"
            allowtransparency="true"
            allow="encrypted-media"
          ></iframe>
  
          <iframe
            className="search__iframe"
            src="https://open.spotify.com/embed/track/1xK1Gg9SxG8fy2Ya373oqb"
            width="250"
            height="250"
            allowtransparency="true"
            allow="encrypted-media"
          ></iframe>
  
          <iframe
            className="search__iframe"
            src="https://open.spotify.com/embed/track/0mDINHqydpdNSyPKPCxB7z"
            width="250"
            height="250"
            allowtransparency="true"
            allow="encrypted-media"
          ></iframe>
  
          <iframe
            className="search__iframe"
            src="https://open.spotify.com/embed/track/07PSGQ8vwLkuvxkWX6QVxY"
            width="250"
            height="250"
            allowtransparency="true"
            allow="encrypted-media"
          ></iframe>
  
          <iframe
            className="search__iframe"
            src="https://open.spotify.com/embed/track/5ovAmYzYLeEmOX6P3bmlzV"
            width="250"
            height="250"
            allowtransparency="true"
            allow="encrypted-media"
          ></iframe>
  
          <iframe
            className="search__iframe"
            src="https://open.spotify.com/embed/track/5H9S7RaxxSAgY77Mb0CSPz"
            width="250"
            height="250"
            allowtransparency="true"
            allow="encrypted-media"
          ></iframe>
  
          <iframe
            className="search__iframe"
            src="https://open.spotify.com/embed/track/6MoTeX5dtuPZ3hwaecuBkz"
            width="250"
            height="250"
            allowtransparency="true"
            allow="encrypted-media"
          ></iframe>
  
          <iframe
            className="search__iframe"
            src="https://open.spotify.com/embed/track/5EEhXlG2oUjNEycxDEsdk0"
            width="250"
            height="250"
            allowtransparency="true"
            allow="encrypted-media"
          ></iframe>
  
          <iframe
            className="search__iframe"
            src="https://open.spotify.com/embed/track/3RQMCAfUMzRCSW93LCRiJy"
            width="250"
            height="250"
            allowtransparency="true"
            allow="encrypted-media"
          ></iframe>
  
          <iframe
            className="search__iframe"
            src="https://open.spotify.com/embed/track/4kd8NJrrfzzPvarSre2yAu"
            width="250"
            height="250"
            allowtransparency="true"
            allow="encrypted-media"
          ></iframe>
  
          <iframe
            className="search__iframe"
            src="https://open.spotify.com/embed/track/2HpbDdD3JLpAPMJFYCeC6o"
            width="250"
            height="250"
            allowtransparency="true"
            allow="encrypted-media"
          ></iframe>
  
          <iframe
            className="search__iframe"
            src="https://open.spotify.com/embed/track/5UKBbglwH7gZEkzfuWGn5J"
            width="250"
            height="250"
            allowtransparency="true"
            allow="encrypted-media"
          ></iframe>
  
          <iframe
            className="search__iframe"
            src="https://open.spotify.com/embed/track/5WYB4aZkflwjAApBoDCYkz"
            width="250"
            height="250"
            allowtransparency="true"
            allow="encrypted-media"
          ></iframe>

        </div>
      </div>
    )
}

export default musicPage;