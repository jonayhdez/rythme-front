import React from "react";
import { useDispatch } from "react-redux";
import { logout } from '../../api/auth';
import { setLocationPage, setUser } from "../../app/appSlice";
import Button from "../../components/Button";
import ConfigurationProfile from '../../components/ConfigurationProfile';
import UserProfile from "../../components/UserProfile";

import './ProfilePage.scss';

const ProfilePage = (props) => {
    const dispatch = useDispatch();

    const logoutUser = async () => {
        await logout();
        dispatch(setUser(null));
        dispatch(setLocationPage('home'));
        props.history.push('/home');
    };

    return(
        <div className='profile'>
            <UserProfile />
            <ConfigurationProfile/>
            <Button style="fanClub" onClick={logoutUser}>Logout</Button>
        </div>
        
    );
};

export default ProfilePage;