const registerUrl = `https://rythme-project.herokuapp.com/auth/register`;
const loginUrl = `https://rythme-project.herokuapp.com/auth/login`;
const checkSessionUrl = `https://rythme-project.herokuapp.com/auth/check_session`;
const logoutUrl = `https://rythme-project.herokuapp.com/auth/logout`;

export const register = async (userData) => {
    const request = await fetch(registerUrl, {
        method :"POST",
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
            "Access-Control-Allow-Credentials": true,
            "Access-Control-Allow-Origin": "*",
        },
        credentials: 'include',
        body: JSON.stringify(userData),
    });

    const response = await request.json();

     if(!request.ok) {
    throw new Error(response.message);
  }

  return response;
};

export const login = async (userData) => {
    const request = await fetch(loginUrl, {
        method :"POST",
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
            "Access-Control-Allow-Credentials": true,
            "Access-Control-Allow-Origin": "*",
        },
        credentials: 'include', //para crear la cookie de sesion debe de estar a true
        body: JSON.stringify(userData),
    });

    const response = await request.json();

    if(!request.ok) {
        throw new Error(response.message);
      }

      return response;

    }

export const checkSession = async () => {
          const request = await fetch(checkSessionUrl, {
              method: 'GET',
              headers: {
                  'Accept': 'application/json',
                  'Content-Type': 'application/json',
                  'Access-Control-Allow-Origin':"*"
              },
              credentials: 'include',
          });

          const response = await request.json();

            if(!request.ok) {
            throw new Error(response.message);
         }
            return response;
}


export const logout = async () => {
    const request = await fetch(logoutUrl, {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin':"*"
        },
            credentials: 'include',
    });

        const response = await request.json();

          if(!request.ok) {
          throw new Error(response.message);
       }
          return response;
}
